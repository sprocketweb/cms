Sprocket \ Cms
=============

This is a Content Management System for Laravel.

It is incomplete and still in alpha.

Please contact Duncan at [duncan@sprocket.co.nz](mailto:duncan@sprocket.co.nz)

## Purpose

Cms will create a administration area for your site that will allow users to edit pages. Pages are stored in a database and can be easily created.

Pages have

- Title
- Content (HTML)
- Description
- Menu name
- Slug for URLs
- Parent

The can also be set to cacheable, searchable, and hidden.

An uploads directory can also be set if it should be different to the default.

## Modules

Other systems for blogs, products, news, etc. can be incorporated into this admin. More on this later.

## Install

Add the package to your composer.json file

	"require": {
		"laravel/framework": "4.1.*",
		"sprocket/cms"
		...
	}

Update composer

	composer update

Add the service provider to your app.php config file

`'Sprocket\Cms\CmsServiceProvider'`


You can publish config files using:

	php artisan config:publish sprocket/cms

and assets:

	php artisan asset:publish

composer.json
--------------------

Autoloads classes from the following directories:

	controllers, exceptions, handlers, helpers, migrations, repo, seeds, services/validation

All files within `helpers` are loaded – this is where command, composer, etc. are set up.

ServiceProvider
--------------------

Adds the 'cms' view Namespace.

Automatically loads files in the Helpers directory –simply drop in a class an off you go.

### Commands
Loads the CreateUser command

### Filters
Adds the isSuper filter

### Helpers
An assortment of functions: isAre, money formatter etc.

### Macros
Adds Form macros.

Commands are located in the Sprocket/Cms/Commands directory but are loaded from within helper/commands.php

## Aliases
Creates aliases for some helpers: ['Cms', 'Dev', 'Time'];

### Cms
Provides Cms related functions, primarily read from Config.

### CmsUser
Methods relating to the CMS user. Such as `CmsUser::isSuper()`

### Dev
Provides some useful developer related functions.

### Time
Provides some useful Time based functions.

Other files can be loaded in the $incl_files – paths from the App src directory.

## Routes

The package will respond to all '/admin' routes.

It will also handle '/login' and '/logout', without the admin prefix.

Helper Functions
----------------
(hasSections()) ? 'yes' : 'no';

Helper Class
----------------

	(Cms::hasSections()) ? 'yes' : 'no';
	(Sprocket\Cms\Helpers\Cms::hasSections()) ? 'yes' : 'no';

	(Cms::hasBlog()) ? 'yes' : 'no';
	(Cms::pagesMenuTitle());


Messages Helper
----------------
There is a messages helper method for retrieving messages from the language file.

	Cms::msg('messages.dashboard.welcome');
	Lang::get('cms::messages.dashboard.welcome');
	trans('cms::messages.dashboard.welcome');

User Command
----------------
There is a Artisan to create new users: `user:create`
It will prompt you for the appropriate details.

To create new commands:
`php artisan command:make UserCreate --path=workbench/sprocket/cms/src/sprocket/cms/commands --namespace=Sprocket\\Cms\\Commands`


Seeding
----------------
`php artisan db:seed --class="Sprocket\Cms\Seeds\DatabaseSeeder"`


Exceptions
----------------

	try {
		throw new ActionNotAllowedException('You are not allowed to do that');
	} catch (ActionNotAllowedException $e) {
		return $e->getMessage();
	}


Views
----------------
These extend @extends('cms::layouts.main')

There are sections for content, title, css, and js.



Auth Controller
-----------------

This triggers user login, logout and badlogin events. These are processed by Handlers/UserEventHandler


## Configuration

### Customisation

A stylesheet can be added to the `custom.css` config file.



## Views
All view files are stored in the Views directory and live in the `Cms` view namespace.


### Nav Bar
The nav bar is generated from sections in the nav config file. This is then used by the Nav ViewComposer.

## Uploads

Each page has a setting for the upload directory. It will use `uploads` if not set.

The redactor plug in will set this based on an input on the page settings page.

## User Settings

There are two levels of user – a normal user and a `Super` user.

A `isSuper` filter is defined.


## Install

	php artisan asset:publish --bench="sprocket/cms-one"

Many parts of the Admin can be customised with the configs. To publish them run:
	php artisan config:publish sprocket/cms

## Dashboard

Views can be added to the Dasboard using the 'dashboard.panels.views' config. Use view composers to load data.
