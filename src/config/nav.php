<?php

return [

	'sections'	=> [
		[ 'title' => 'Pages', 'url' => 'pages']
	],

	'avatar'	=> [
		'default' => '/packages/sprocket/cms/img/cog-40-40.png'
	]

];
