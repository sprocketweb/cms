<?php

return [
	'minheight' => 720,
	'css'		=> '/assets/admin/css/editor.css',
	'preview'	=> '/assets/admin/css/preview.css',
	'buttons'	=> "'formatting', '|', 'bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'image', '|', 'video', 'file', 'table', 'link', '|', 'alignment'",

	'autosave' => [
		'use'		=> 'false',
		'interval' 	=> '60',
	],

	'convertimagelinks'	=> 'true',
	'convertvideolinks'	=> 'true',
	'linkanchor'		=> 'true',
	'observeimages'		=> 'true',
	'focus'				=> 'false',
	'fixed'				=> 'true',
	'iframe'			=> 'true',
	'toolbarfixedbox'	=> 'true',
	'toolbarfixedtopoffset'	=> '60',

	'file'	=> [
		'fileUpload'	=>  '/file_upload.php'
	],

];
