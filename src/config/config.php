<?php

return [

	'version'		=> '0.1.0 alpha',
	'has_sections'	=> false,
	'has_media'		=> false,

	'site'	=> [
		'tld'			=> 'dev.cms-one',
		'name'			=> 'Your Web Site'
	],

	'custom' => [
		'css'	=> '/css/admin.css' /* add an additional stylesheet */
	],

	'rollbacks'		=> false,
	'drafts'		=> false,
	'autosave'		=> false,
	'create_new'	=> false,

	'assets'		=> [ // include, overides
		'dir'	=> '/assets/admin',
		'css'	=> '/assets/admin/css/',
		'img'	=> '/assets/admin/img/',
		'js'	=> '/assets/admin/js/',
	],



];
