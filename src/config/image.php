<?php

return [

	// validation rules
	'rules'		=> 'max:1536|image|mimes:jpg,jpeg,gif,png',

	'url' => [
		// upload path for editor
		'upload'	=> '/admin/upload/image',
		// listing path for editor
		'listing'	=> '/admin/image/listing',
	],

	'dir' => [
		// image directory from public
		'images'	=> '/assets/images',
		// default upload directory within asset images directory
		'default' 	=> '/uploads',
	],

];
