<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--[if lt IE 9]>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<title>Admin</title>

	<link rel="stylesheet" href="/packages/sprocket/cms/vendor/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/packages/sprocket/cms/vendor/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="/packages/sprocket/cms/css/sitewide.css" />

	@yield('css')

	@if(Config::get('cms::custom.css'))
	<link rel="stylesheet" href="{{ Config::get('cms::custom.css') }}" />
	@endif

</head>

<body>
@include('cms::layouts.nav_wrapper')

<div class="container content-wrap">
	<div class="row">
		<div class="col-lg-12 content-main">
		<div class="header">
			@yield('header')
		</div>
			@include('cms::layouts.messages')
			@yield('content')
		</div>
	</div><!--row-->
</div><!--container-->

	<script src="/packages/sprocket/cms/vendor/jquery/jquery-1.11.0.min.js"></script>
	<script src="/packages/sprocket/cms/vendor/bootstrap/js/bootstrap.min.js"></script>
	@yield('js')

<script>
$('.dropdown').hover(
	function(){$(this).addClass('down').find('.dropdown-menu').stop(true, true).delay(50).slideDown('fast');},
	function(){$(this).find('.dropdown-menu').stop(true, true).delay(150).slideUp('fast', function(){$('.dropdown').removeClass('down');});}
);

$('.alert-dismissable').on('click',function(){
	$(this).slideUp();
});
</script>
</body>
</html>
<!-- {{ Dev::getEnv() }} -->
