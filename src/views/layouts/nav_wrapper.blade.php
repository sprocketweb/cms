<div class="navbar navbar-default navbar-fixed-top navbar-main" role="navigation">

<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
</div><!--navbar-header-->
<div class="container">
<div class="row">

<div class="collapse navbar-collapse">

    <div class="dropdown pull-right">
      <a id="nav-avatar" role="button" data-toggle="dropdown" data-target="#" href="#">
        <!-- <img src="/packages/sprocket/cms/img/duncan.jpg" class="img-circle"> -->
        <img src="{{ Config::get('cms::nav.avatar.default') }}" class="img-circle">
      </a>
      <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          @if(Auth::user()->super)
              <li><a href="{{ route('admin.super') }}">Super</a></li>
          @endif
          <li><a href="{{ route('admin.help') }}">Help</a></li>
          <li><a href="{{ route('logout') }}">Logout</a></li>
      </ul>

    </div><!--dropdown-->

  @if(Auth::user()->super)
  <span class="pull-right cms-nav-message">
  {{ Config::get('cms::super.nav.message', 'default install') }}
  </span>
  @endif

@include('cms::layouts.nav')
</div>
</div>
</div><!--nav-collapse-->
</div><!--navbar-->
