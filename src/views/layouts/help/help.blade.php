<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Help</title>
	<?php echo Asset::styles(); ?>
</head>
<body>

	<div class="row span9">
		<div class="content">
			{{ $content }}
		</div><!-- content -->
	</div><!-- row -->

<div id="help-button-bar">
{{ HTML::mailto('support@sprocket.co.nz','Send an email',array('class'=>'btn')) }}
</div>

<?php echo Asset::container('footer')->scripts(); ?>
</body>
</html>
