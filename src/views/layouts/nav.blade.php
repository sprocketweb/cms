<ul class="nav navbar-nav">
	<li><a href="/admin"><i class="fa fa-dashboard fa-2x"></i> </a></li>
@foreach($sections as $section)
	<li><a href="/admin/{{$section['url']}}">{{$section['title']}}</a></li>
@endforeach
</ul>
