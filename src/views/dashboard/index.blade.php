@extends('cms::layouts.main')

@section('header')
<h1>{{ Cms::siteName() }}</h1>
@stop

@section('content')
@if((Session::has('login_message')))
<div class="alert alert-success">{{ Session::get('login_message') }}</div>
@endif

<!-- panels -->
<div class="row">

@foreach($panels as $view)
	@include ($view)
@endforeach

</div><!--panels-->
@stop

@section('js')
@stop
