# Help!

There are two parts of your web site – the 'public' part, that your visitors see, and the 'admin' part, that allows you to make changes to the site.

Note: if you try to access a page within the administration area when you are not logged it, it will direct you to the log in page, then onto the requested page.

You will be shown the Dashboard, which will list your pages and recent blog posts. It will also tell you when you last logged in – this is so you can keep an eye on unwanted log ins.

Along the top are links to the sections of your admin area.

## Logging Out

To log out choose 'log out' from the cog in the upper right corner. You will be automatically logged out after a period of inactivity but it is always a good idea to log out manually to ensure against unauthorised access.


## Pages

To edit a web page click on the 'Pages' link at the top or on a page listed in the panel. This will show you the page editor with your selected page.

The 'Pages' section will show you a list of the pages within your web site.

It will show you the page title, link and when it was last edited. The link will take you to the 'public' web site. You can preview a page with the preview button or edit it with the 'Edit' button.

### Editing Pages
Edit the text as required.

The Settings tab gives you options for options like the Menu text, sort order, etc.

Once you have finished editing the page click the 'Save' button. If you do not want to save the changes then click 'Cancel'

To the right there are some 'quick action' buttons.

From left to right:
Reload - this will reload the current page, discarding any changes you may have made. 
Cancel - takes you back to the previous page without saving changes. 
Quick Save - this will save changes you have made while allowing you to continue editing. Save - saves the page and takes you to the Pages section.

Reload and Cancel highlight in red as the changes are destructive and you can lose changes to your web page.

## Blog

Your site has a blogging system built into it. It works in a similar way to the Page editor but with some differences.

A Blog Post is similar to a page but won't show up in the main navigation menu. Posts also have some categorising features to allow your visitors to find content they are interested in.

Each post belongs to a single category. These can be created and edited in your admin. A post can only ever belong to one category.

Posts also have one or more tags – these are added arbitrarily and a post can have as many as you like. A 'tag cloud' will be made allowing visitors to view other related posts.

The Blog admin page will list the posts, starting with the most recent.

Click on the 'New Post' button, this will take you to the post create page.

You have options to add a title, the post body and tags.

Tags are separated by commas and should automatically split up.

A blog post can be published immediately or you can save it as a draft to published later.

Formatting options are the same as pages.
