@extends('cms::help.template')

@section('header')
	@parent
	<h1>Blog Help</h1>
@stop

@section('article')

<h2>Blogging System</h2>

<p>Your site has a blogging system built into it. It works in a similar way to the Page editor but with some differences.</p>

<p>A Blog Post is similar to a page but won&rsquo;t show up in the main navigation menu (but the 'blogs' section will). They also have additional features that allow visitors to find related blog entries.</p>

<h3>Categories</h3>

<p>Posts also have some categorising features to allow your visitors to find content they are interested in. Each post belongs to a single category. These can be created and edited in your admin. You can have as many categories as you need but it might be best to have fewer categories with more posts in them rather than many categories with few posts.</p>

<p>The Blog admin page will list the posts, starting with the most recent.</p>

<p>Click on the &lsquo;New Post&rsquo; button, this will take you to the post create page.</p>

<p>You have options to add a title, the post body and tags.</p>

<h3>Tags</h3>

<p>Posts also have one or more tags – these are added arbitrarily and a post can have as many as you like. A &lsquo;tag cloud&rsquo; will be made allowing visitors to view other related posts.</p>

<p>Tags are separated by commas and should automatically split up.</p>

<p>A blog post can be published immediately or you can save it as a draft to published later.</p>

<p>Formatting options are the same as pages.</p>

@stop
