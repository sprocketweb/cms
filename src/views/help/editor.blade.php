@extends('cms::help.template')

@section('header')
	@parent
	<h1>Page Editor Help</h1>
@stop

@section('article')
<h2>Page Editor</h2>
<p>To edit a web page click on the &lsquo;Pages&rsquo; link at the top or on a page listed in the panel. This will show you the page editor with your selected page.</p>

<p>The &lsquo;Pages&rsquo; section will show you a list of the pages within your web site.</p>

<p>It will show you the page title, link and when it was last edited. The link will take you to the &lsquo;public&rsquo; web site. You can preview a page with the preview button or edit it with the &lsquo;Edit&rsquo; button.</p>

<h3>Editing Pages</h3>

<p>Edit the text as required.</p>

<p>The Settings tab gives you options for options like the Menu text, sort order, etc.</p>

<p>Once you have finished editing the page click the &lsquo;Save&rsquo; button. If you do not want to save the changes then click &lsquo;Cancel&rsquo;</p>

<h3>&lsquo;Quick Action&rsquo; buttons</h3>

<p>To the right there are some &lsquo;quick action&rsquo; buttons.</p>

<img src="/packages/sprocket/cms/help/1/cms-quick-action-buttons.png" width="135" height="32">

<p>From left to right:</p>

<dl>
<dt>Reload</dt>
	<dd>this will reload the current page, discarding any changes you may have made </dd>
<dt>Cancel</dt>
	<dd>takes you back to the previous page without saving changes </dd>
<dt>Quick Save</dt>
	<dd>this will save changes you have made while allowing you to continue editing</dd>
<dt> Save</dt>
	<dd>saves the page and takes you to the Pages section</dd>
</dl>

<p>Reload and Cancel highlight in red as the changes are destructive and you can lose changes to your web page.</p>

@stop
