<div class="btn-group pull-right">
	<a href="{{ route('admin.help') }}" class="btn btn-default">Help Home</a>
	<a href="{{ route('admin.help.editor') }}" class="btn btn-default">Editor</a>
	<a href="{{ route('admin.help.blog') }}" class="btn btn-default">Blog</a>
	<a href="{{ route('admin.help.support') }}" class="btn btn-default">Support</a>
</div>
