@extends('cms::layouts.main')

@section('header')
@include('cms::help.nav')
@stop

@section('content')
<article class="col-lg-8" id="help">
	@yield('article')
</article><!--col-lg-8 col-lg-offset-1 -->

<div class="col-lg-3 col-lg-offset-1">
	<div id="jump" class="list-group"></div>
</div>
@stop

@section('js')
<script src="/packages/sprocket/cms/help/1/help.js"></script>
@stop
