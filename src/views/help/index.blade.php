@extends('cms::help.template')

@section('header')
	@parent
	<h1>Help</h1>
@stop

@section('article')
<h2>Admin Area</h2>
<p>There are two parts of your web site – the &lsquo;public&rsquo; part, that your visitors see, and the &lsquo;admin&rsquo; part, that allows you to make changes to the site. The public part is sometimes called the 'front-end' and the administration area is sometimes called the 'back-end'.
</p>

<p>Note: if you try to access a page within the administration area when you are not logged it, it will direct you to the log in page, then onto the requested page.</p>

<h3>Dashboard</h3>

<p>The Dashboard lists your pages and recent blog posts. It will also tell you when you last logged in – this is so you can keep an eye on unwanted log ins. New panels will be added to the dashboard over time.</p>

<h3>Navigation</h3>

<p>Along the top are links to the sections of your admin area. You can use these to move from one section to the next.</p>

<p>If you become lost within the admin area and wish to return to the 'home' page for a section then simply click the appropriate link in the navigation bar.</p>

<p>If you click on one of these without saving changes to pages or blog posts then you will likely lose any changes you have made – so be careful!</p>

<h3>Logging Out</h3>

<p>To log out choose &lsquo;log out&rsquo; from the cog in the upper right corner. You will be automatically logged out after a period of inactivity. It is recommended that you log out manually to ensure against unauthorised access.</p>
@stop
