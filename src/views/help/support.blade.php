@extends('cms::help.template')

@section('header')
	@parent
	<h1>Support</h1>
@stop

@section('article')
<div class="col-lg-8 col-lg-offset-1">
<p>Soon you will be able to ask a question or request assistance directly from here. In the meantime, please use email:</p>
<a href="mailto:info@sprocket.co.nz" class="btn btn-default btn-lg btn-info"><i class="fa fa-envelope"></i> info@sprocket.co.nz</a>
</div>
@stop
