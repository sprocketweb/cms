<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $page->title }}</title>
	{{ HTML::style("packages/sprocket/cms/vendor/bootstrap/css/bootstrap.min.css") }}
</head>
<body>
{{ $page->content }}
</body>
</html>
