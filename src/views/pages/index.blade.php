@extends('cms::layouts.main')

@section('css')
@if(Config::get('cms::page.editor.preview'))
<link rel="stylesheet" href="{{ Config::get('cms::page.editor.preview') }}" />
@endif
@stop


@section('header')
@if(CmsUser::isSuper())
<a href="{{ action('Sprocket\Cms\PageController@create')}}" class="btn btn-default pull-right"><i class="fa fa-plus"></i>	Add a new page</a>
@endif
<h1>All Pages</h1>
@stop

@section('content')
@if ($pages->count())

<table class="table table-bordered table-hover table-striped">
<tbody>

@foreach($pages as $page)
<tr>
	<td>
		<strong>{{ $page->title }}</strong>
		<br>

		<a href="http://{{ Config::get('cms::site.tld') }}/{{ $page->slug }}">
			<i class="fa fa-link"></i>
			{{ $page->slug }}
		</a>

	</td>
	<td>
		{{ Time::daysAgo($page->updated_at) }}
	</td>
	<td>
	@if(Auth::user()->super)
	{{ Form::open( [
		'url'=> URL::route('admin.pages.destroy',[$page->id]),
		'method' => 'delete',
		'class' => 'form-inline',
		'role' => 'form'
		]) }}
	<button class="btn btn-danger pull-right pages-btn-delete"><i class="fa fa-exclamation-triangle"></i> Delete</button>
	{{ Form::close() }}
	@endif
	<a href="{{ action('Sprocket\Cms\PageController@edit',$page->id) }}" class="btn btn-info btn-small"><i class="fa fa-pencil"></i> Edit</a>

	<a href="{{ action('Sprocket\Cms\PageController@show',$page->id) }}" class="btn btn-info btn-small pages-btn-preview"><i class="fa fa-eye"></i> Preview</a>

	</td>
</tr>
@endforeach
</tbody>
</table>

@include('cms::pages.preview_modal')
{{ Cms::totals('page', $pages->count(), 'footer-details') }}

@else
	<p class="lead">There are no pages.</p>
@endif

@stop

@section('js')
<script>
$('.pages-btn-preview').on('click',function(e){
	e.preventDefault();
	$url = $(this).attr('href');
	var page_id = $url.split(/[//]+/).pop();
	$('#pages-modal-btn-edit').attr('href','/admin/pages/'+page_id+'/edit');
	$('.modal .modal-body').load($url,
		function(e){
			$('.modal').modal({
				'show' : true,
				'backdrop' : true
			});
	});
});
</script>
@stop
