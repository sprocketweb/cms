<div class="form-group">
	{{ Form::label('title') }}
	{{ Form::text('title',null,['class'=>'form-control']) }}
	<span class="help-block">Used on web browser windows and search engines results pages (max 70 characters).</span>
</div>

<div class="form-group">
	{{ Form::label('description') }}
	{{ Form::textarea('description',null,['class'=>'form-control', 'rows'=>'3']) }}
	<span class="help-block">A brief overview of the page. Useful for search engines.</span>
</div>

<div class="row form-row">

	<div class="col-md-4">
		{{ Form::label('visible') }}
		{{ Form::select('hidden',['Visible','Hidden'],null,['class'=>'form-control']) }}
		<span class="help-block">Available to the public</span>
	</div>

	<div class="col-md-4">
		{{ Form::label('menu') }}
		{{ Form::text('menu',null,['class'=>'form-control']) }}
		<span class="help-block">Used with site navigation</span>
	</div>

	<div class="col-md-2">
		{{ Form::label('order') }}
		{{ Form::text('order',null,['class'=>'form-control']) }}
		<span class="help-block">Order in menus</span>
	</div>

</div>

@if(CmsUser::isSuper())
<div class="row form-row-group">

	<div class="form-row">
		<div class="col-md-3">
			{{ Form::label('cacheable') }}
			{{ Form::select('cacheable',['Cacheable','Not cacheable'],null,array('class'=>'form-control')) }}
		</div>

		<div class="col-md-3">
			{{ Form::label('searchable') }}
			{{ Form::select('searchable',['Searchable','Not searchable'],null,array('class'=>'form-control')) }}
		</div>

	<div class="col-md-3">
		{{ Form::label('Parent') }}
		{{ Form::text('parent',null,['class'=>'form-control']) }}
	</div>

	<div class="col-md-3">
		{{ Form::label('Type') }}
		{{ Form::text('type',null,['class'=>'form-control']) }}
	</div>
	</div><!--row-->


	<div class="form-row">
		<div class="col-md-6">
			{{ Form::label('slug') }}
			<div class="input-group">
			  <span class="input-group-addon">{{ Config::get('cms::site.tld') }}/</span>
			{{ Form::text('slug',null,['class'=>'form-control']) }}
			</div>
		</div>


		<div class="col-md-6">
			{{ Form::label('Upload Directory') }}
			{{ Form::text('upload_dir',null,['class'=>'form-control', 'id'=>'upload_dir', 'placeholder' => 'uploads']) }}
			<span class="help-block">Directory for uploaded images</span>
		</div>

	</div><!--row-->

</div><!-- form-row-group -->
@endif
