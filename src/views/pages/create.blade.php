@extends('cms::layouts.main')

@section('header')
<a href="{{ action('Sprocket\Cms\PageController@index')}}" class="btn btn-default pull-right"><i class="fa fa-ban"></i> Cancel</a>
<h1>Create a new page</h1>
@stop

@section('css')
<link rel="stylesheet" href="/packages/sprocket/cms/vendor/redactor/redactor.css"/>
@stop

@section('content')

@include('cms::pages.errors')

<div class="row col-md-10 col-md-offset-1">

{{ Form::open( [
	'url'=> URL::route('admin.pages.index'),
	'method' => 'post'
	]) }}

@include('cms::pages.page_settings')

{{ Form::textarea('content',null,['class'=>'redactor']) }}

@include('cms::layouts.forms.basicactions')

{{ Form::close() }}
@stop

@section('js')
<script src="/packages/sprocket/cms/vendor/redactor/redactor.min.js"></script>
@include('cms::pages.js.edit')
@stop
