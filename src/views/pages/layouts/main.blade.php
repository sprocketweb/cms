@extends('cms::layouts.main')

@section('title')
Sprocket\CMS | Pages
@stop

@section('scripts')
	@parent
	{{ HTML::script("packages/sprocket/cms/vendor/redactor/redactor/redactor.js") }}
	{{ HTML::script("packages/sprocket/cms/vendor/redactor/fullscreen/fullscreen.js") }}
	{{ HTML::script("packages/sprocket/pages/js/pages.js") }}
@stop
