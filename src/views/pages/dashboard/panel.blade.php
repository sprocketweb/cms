<div class="col-sm-4">
<div class="panel panel-primary dashboard-panel">

	<div class="panel-heading">
		<h4 class="panel-title">
			<a href="{{ action('Sprocket\Cms\PageController@index') }}">Pages</a>
		</h4>
	</div>

	<div class="list-group">
		@foreach($pages as $page)
			<a href="{{ action('Sprocket\Cms\PageController@edit',$page->id) }}" class="list-group-item">
			<i class="fa fa-pencil pull-right"></i>
			{{ $page->title }}
			</a>
		@endforeach
	</div><!--list-group-->

</div><!--panel-->
</div><!--col-->
