@extends('cms::layouts.main')

@section('title')
Welcome to Sprocket\CMS
@stop

@section('content')
<h1>{{ $page->title }}</h1>
{{ $page->content }}
@stop
