<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Login</title>
@section('head')
<link rel="stylesheet" href="/packages/sprocket/cms/vendor/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="/packages/sprocket/cms/vendor/font-awesome/css/font-awesome.min.css" />
@show
<style>
	body {
		padding-top: 12em;
		background: #dfdfdf;
	}
	.form-signin {
		margin: 10px auto;
		padding: 5px 15px 0;
		overflow: auto;
	}
	.form-signin .form-control {
		position: relative;
		font-size: 16px;
		height: auto;
		padding: 8px 10px;
	}
	.form-signin input {
		margin-bottom: .8em;
	}
	.account-wall {
		margin-top: 40px;
		padding: 30px 0px 3px 0px;
		background: rgba(0,0,0,.9);
		background: rgba(255,255,255,.4);
		background-color: #f7f7f7;
		box-shadow: -0px 1px 2px rgba(0, 0, 0, 0.2);
		border-radius: 2%;
	}
	.account-wall .alert {
		border-radius: 0;
		text-align: center;
	}
	img {
		margin: 0 auto 20px;
		display: block;
	}
	h1 {
		display: none;
	}
	img {
	  -webkit-transition:all 0.5s ease-in;
	  transition:all 0.5s ease-in;
	}
	img:hover {
	  -webkit-transform:rotate(360deg);
	  transform:rotate(360deg);
	}
</style>
<link rel="stylesheet" href="{{ Config::get('cms::assets.css').'login.css' }}">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h1 class="text-center">Log in</h1>
			<div class="account-wall">
				<image src="/packages/sprocket/cms/img/cog-108-108.png" />
	@include('cms::auth.messages')
				{{ Form::open(array('url'=>'login','method'=>'post','class'=>'form-signin')) }}
				{{ Form::email('email',NULL,array('class'=>'form-control clearfix', 'placeholder'=>'Email','required'=>'required','autofocus'=>'autofocus')) }}
				{{ Form::password('password',array('class'=>'form-control clearfix', 'placeholder'=>'Password','required'=>'required')) }}
				{{ Form::submit('Log in',array('class'=>'btn btn-primary btn-lg btn-block pull-right clearfix')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

<script src="/packages/sprocket/cms/vendor/jquery/jquery-1.11.0.min.js"></script>
<script>
jQuery(document).ready(function() {
setTimeout(function() {
	$('.alert').slideUp('slow');
}, 4000);
});

$('.form-signin').on('submit',function(e){
	$('.account-wall').fadeOut('fast');
});
</script>
</body>
</html>
