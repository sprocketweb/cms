<?php namespace Sprocket\Cms\Handlers;

use Sprocket\Cms\Helpers\CmsUser;
use \Input;
use \Log;
use \Request;

class UserEventHandler {

	public function subscribe($events)
	{
		$events->listen('user.dashboard', 	'Sprocket\Cms\Handlers\UserEventHandler@onDashboard');
		$events->listen('user.login', 		'Sprocket\Cms\Handlers\UserEventHandler@onLogin');
		$events->listen('user.logout', 		'Sprocket\Cms\Handlers\UserEventHandler@onLogout');
		$events->listen('user.badlogin', 	'Sprocket\Cms\Handlers\UserEventHandler@onBadLogin');
	}

	public function onLogin($user)
	{
		CmsUser::loginMessage($user);
		CmsUser::updateLogin($user);
		Log::info($user->email . ' logged in');
	}

	public function onLogout($user)
	{
		Log::info($user->email . ' logged out');
	}

	public function onBadLogin($email)
	{
		Log::warning('Bad Login', [
			'email'					=> $email,
			'IP'					=> Request::getClientIp(),
			'HTTP_TRUE_CLIENT_IP'	=> Request::server('HTTP_TRUE_CLIENT_IP')
		]);
	}

}
