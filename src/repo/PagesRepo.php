<?php namespace Sprocket\Cms\Repo;

use Sprocket\Cms\Repo\PagesDb as Page;
use Sprocket\Cms\Repo\PagesRepositoryInterface;
use Sprocket\Cms\Repo\DbRepository;

class PagesRepo extends DbRepository implements PagesRepositoryInterface {

	protected $model;

	public function __construct(Page $model)
	{
		$this->model = $model;
	}

	public function getAll()
	{
		return $this->model->orderBy('order')->get();
	}

}
