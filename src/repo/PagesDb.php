<?php namespace Sprocket\Cms\Repo;

use \Eloquent;

class PagesDb extends Eloquent {

	protected $table = 'pages';

	protected $guarded = array();

	protected $softDelete = true;

	/* 'title','content','slug','menu','description','section','published','searchable','autosave','cacheable', */

}
