<?php namespace Sprocket\Cms\Repo;

interface PagesRepositoryInterface {

	public function getAll();

	public function getById($id);

	public function delete($id);

}
