<?php namespace Sprocket\Cms\Repo;

interface Repository extends RepositoryInterface {

	public function getAll();

}
