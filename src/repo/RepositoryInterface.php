<?php namespace Sprocket\Cms\Repo;

interface RepositoryInterface {

	public function getAll();
	public function get($id);
	public function delete($id);

}
