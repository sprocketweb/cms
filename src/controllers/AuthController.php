<?php namespace Sprocket\Cms;

use \Auth;
use \Event;
use \Input;
use \Redirect;
use \View;

class AuthController extends BaseController {

	/**
	 * default admin home page
	 * @var string
	 */
	private $adminHome = '';


	public function __construct()
	{
		$this->adminHome = '/admin/dashboard';
	}

	/**
	 * Login form
	 *
	 * @return Response
	 */
	public function showLogin()
	{
		return View::make('cms::auth.login');
	}

	/**
	 * Process login attempt
	 * @return mixed view or redirect
	 */
	public function processLogin()
	{
		$userdata = array(
			'email' 	=> Input::get('email'),
			'password'	=> Input::get('password')
		);
		if ( Auth::attempt($userdata) ) return $this->goodLogin();

		return $this->badLogin();
	}

	/**
	 * logs user out
	 * can be used in front end, logout&back=1
	 * @return redirect log in or prev page
	 */
	public function showLogout()
	{
		Event::fire('user.logout',Auth::user());
		Auth::logout();

		if (isset($_GET['back'])) return Redirect::back();

		return Redirect::to('login')->with('message','You have been logged out.');
	}


	/**
	 * successfully logged in
	 * @return redirect to intended or dashboard
	 */
	public function goodLogin()
	{
		Event::fire('user.login', Auth::user());

		return Redirect::intended($this->adminHome);
	}

	/**
	 * unsuccessful log in
	 * @return redirect back to login with error
	 */
	public function badlogin()
	{
		Event::fire('user.badlogin', Input::get('email'));

		return Redirect::to('login')->with('error','There was an error with your log in.');
	}

}
