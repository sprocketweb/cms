<?php namespace Sprocket\Cms;

use \View;

class SuperController extends \BaseController {

	public function __construct()
	{
		$this->beforeFilter('isSuper');
	}

	public function index()
	{
		return View::make('cms::super.index');
	}

	public function getBackup()
	{
		return 'back up';
	}

}
