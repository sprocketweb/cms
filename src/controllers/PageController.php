<?php namespace Sprocket\Cms;

use \Event;
use \Input;
use \Redirect;
use \Request;
use \View;
use \Sprocket\Cms\Repo\PagesRepo as Page;
use \Sprocket\Cms\Services\Validation\PageValidator as Validator;

class PageController extends \BaseController {

	protected $page;
	protected $validator;

	public function __construct(Page $page, Validator $validator)
	{
		$this->page = $page;
		$this->validator = $validator;

		$this->beforeFilter('isSuper', array('except' => ['index','edit','update','show']) );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = $this->page->getAll();

		return View::make('cms::pages.index',compact('pages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('cms::pages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if ( ! $this->validator->validate(Input::all()) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		$page = $this->page->create(Input::all());

		if ($page->save()) return Redirect::route('admin.pages.index')->with('message','Page was created');

		return Redirect::back()->withInput()->withErrors($page->errors);
	}

	/**
	 * Display the specified resource.
	 * Shows just the content to ajax requests
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$page = $this->page->getById($id);

		return Request::ajax() ? $page->content : View::make('cms::pages.preview.index',compact('page'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = $this->page->getById($id);

		return View::make('cms::pages.edit',compact('page'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if ( ! $this->validator->validate(Input::all()) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		$this->page->getById($id)->fill(Input::all())->save();

		return Redirect::route('admin.pages.index')->with('message','The page has been updated.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->page->delete($id);

		return Redirect::route('admin.pages.index')->with('message', 'The page was deleted.');
	}

}
