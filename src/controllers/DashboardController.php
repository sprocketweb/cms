<?php namespace Sprocket\Cms;

use \Config;
use \View;

class DashboardController extends \BaseController {

	public function __construct()
	{
	}

	public function index()
	{
		$panels = Config::get('cms::dashboard.panels.views');

		return View::make('cms::dashboard.index', compact('panels'));
	}

}
