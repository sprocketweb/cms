<?php namespace Sprocket\Cms;

use \Sprocket\Cms\Repo\PagesRepo as Pages;
use \View;

class HelpController extends \BaseController {

	/**
	 * Pages Repository
	 *
	 * @var page
	 */
	protected $page;

	public function __construct(Pages $page)
	{
		$this->page = $page;
	}

	public function index()
	{
		return View::make('cms::help.index');
	}

	public function editor()
	{
		return View::make('cms::help.editor');
	}

	public function blog()
	{
		return View::make('cms::help.blog');
	}

	public function support()
	{
		return View::make('cms::help.support');
	}

}
