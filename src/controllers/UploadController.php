<?php namespace Sprocket\Cms;

use \Config;
use \Event;
use \File;
use \Input;
use \Log;
// use Sprocket\Pages\Services\Validation\PageValidator as Validator;

class UploadController extends \BaseController {

	private $dest_dir;
	private $errorMessage;

	public function __construct()
	{
	}

	public function image()
	{
		$file = Input::file('file');

		if ( ! $this->validFile()) return $this->errorMessage;

		$dir = $this->getUploadDirectory();

		$asset_dir = Config::get('cms::image.dir.images','/assets/images');

		$this->dest_dir = $asset_dir .'/'. \Str::slug($dir);
		$this->checkDestinationDirectory();

		$name = $this->createName();

		$createdFile = public_path()."{$this->dest_dir}/{$name}";

		File::move( $_FILES['file']['tmp_name'], $createdFile);

		chmod($createdFile, 0755);

Log::info('File uploaded: ' . $createdFile);

		return stripslashes(json_encode(['filelink' => $this->dest_dir.'/'.$name]));
	}


	private function validFile()
	{
		$rules = Config::get('cms::image.rules','max:1024|image');
		$validation = \Validator::make(Input::all(),['file'=>$rules]);

		if ($validation->fails())
		{
			$msg = $validation->messages()->first('file');
			Log::info("Bad Upload: $msg");
			$this->errorMessage = stripslashes(json_encode(["error" => $msg]));
			return false;
		}
		return true;
	}

	private function createName()
	{
		$pathinfo = pathinfo($_FILES['file']['name']);

		return \Str::slug($pathinfo['filename']).'.'. strtolower($pathinfo['extension']);
	}


	private function checkDestinationDirectory()
	{
		if ( ! File::isDirectory(public_path() . $this->dest_dir ))
			{
			if ( ! File::makeDirectory(public_path() . $this->dest_dir) )
				Log::info("Error making directory: ".$this->dest_dir);
			}
	}


	/**
	 * sets the upload directory
	 * uses upload_dir if set by request
	 * @return string
	 */
	private function getUploadDirectory()
	{
		$dir = $_POST['upload_dir'];
		$default_dir = Config::get('cms::image.dir.default','/uploads');

		return ( ! empty($dir)) ? '/'. $dir : $default_dir;
	}






	public function listing()
	{
		// 	{"thumb": "/tests/_images/1_m.jpg", "image": "/tests/_images/1.jpg", "title": "Image 1", "folder": "Folder 1" },
		$images = [];
		$img_dir = public_path() . '/assets/images';

		$dirs = File::directories($img_dir);

		foreach ($dirs as $dir) {

			$files = File::files($dir);

			foreach ($files as $file) {

				$path_parts = pathinfo($file);

				// $directory_array = explode('/', $path_parts['dirname']);
				$parent = last(explode('/', $path_parts['dirname']));
				// $parent = last($directory_array);

				$images[] = [
					'image'		=> '/assets/images/'.$parent.'/'.$path_parts['basename'],
					'thumb'		=> '/assets/images/'.$parent.'/'.$path_parts['basename'],
					'folder'	=> $parent
				];

			}

		}

		return \Response::json($images);
		// return stripslashes(json_encode($files));
	}


}
