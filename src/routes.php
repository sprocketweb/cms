<?php
Route::post('/login','Sprocket\Cms\AuthController@processLogin');
Route::get('/login',['uses'=>'Sprocket\Cms\AuthController@showLogin','as'=>'login']);
Route::get('/logout',['uses'=>'Sprocket\Cms\AuthController@showLogout','as'=>'logout']);

Route::group(['prefix' => 'admin','before'=>'auth'], function()
{
	Route::get('/',function(){ return Redirect::route('dashboard'); });

	Route::resource('pages','Sprocket\Cms\PageController');
	Route::post('/upload/image', ['uses'=>'Sprocket\Cms\UploadController@image', 'as'=>'admin.image.upload']);
	Route::get('/image/listing', ['uses'=>'Sprocket\Cms\UploadController@listing', 'as'=>'admin.image.listing']);

	Route::get('help', ['uses'=>'Sprocket\Cms\HelpController@index', 'as'=>'admin.help']);
	Route::get('help/editor', ['uses'=>'Sprocket\Cms\HelpController@editor', 'as'=>'admin.help.editor']);
	Route::get('help/blog', ['uses'=>'Sprocket\Cms\HelpController@blog', 'as'=>'admin.help.blog']);
	Route::get('help/support', ['uses'=>'Sprocket\Cms\HelpController@support', 'as'=>'admin.help.support']);

	Route::get('super',['uses'=>'Sprocket\Cms\SuperController@index', 'as'=>'admin.super']);

	Route::get('dashboard', ['uses'=>'Sprocket\Cms\DashboardController@index', 'as'=>'dashboard']);

});

