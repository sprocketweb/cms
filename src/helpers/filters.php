<?php

$this->app['router']->filter('isSuper', function()
{
    if ( ! CmsUser::isSuper() )
    {
        return $this->app['redirect']
        	->route('dashboard')
        	->with('message','You don\'t have enough access do that, sorry.');;
    }
});
