<?php

/**
 * returns 'is' or 'are' based on item count
 * @param  integer $count
 * @return string
 */
function isAre($count = 1)
{
	return ($count != 1) ? 'are' : 'is';
}


/**
 * returns money as a formatted string
 * @param  integer $ammount
 * @param  string $symbol  currency symbol
 * @return string
 */
function money($ammount, $symbol = '$')
{
	return $symbol . money_format('%i', $ammount);
}
