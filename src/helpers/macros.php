<?php

$this->app['form']->macro('actions',function($type='basic')
{
	// return include('cms::layouts.forms.basicactions');
	// return \View::include('cms::layouts.forms.basicactions');
	// return $this->app['view']->render('cms::layouts.forms.basicactions');
	// return 'form actions';
	/*
	<div class="form-actions clearfix">
	{{ HTML::link(Request::referrer(),'Cancel', array('class' => 'btn btn-large', 'accesskey' => '.')) }}
	{{ Form::submit('Save', array('class' => 'btn btn-large btn-success pull-right', 'accesskey' => 's')) }}
	</div>
	*/
});

// http://nielson.io/2014/02/handling-checkbox-input-in-laravel/
$this->app['form']->macro('cms_check_box', function($name, $value, $checked = false, $label = false)
{
	$checked = ($checked)?'checked':'';
	$tmpl = '<input type="checkbox" name="%s" value="%s" %s>';
	$html = sprintf($tmpl, $name, $value, $checked );

	if($label) {
		$tmpl_label = '<label>%s %s</label>';
		$html = sprintf($tmpl_label, $html, $label);
	}
	$opposite = $checked ? 'false': 'true';
	$hidden = '<input type="hidden" value="'.$opposite.'">';
	return $hidden . $html;
});


$this->app['form']->macro('test',function($type='basic'){
	return 'test form macro';
});

$this->app['form']->macro('UserSelect',function($selected = null, $elName = 'user'){

	$selected = $selected? $selected: Auth::user()->id;

	// $html =	"<select id=\"$elName\" name=\"$elName\" class=\"form-control\">";
	$html = sprintf('<select id="%s" name="%s" class="form-control">',$elName,$elName);
	$tmpl = '<option value="%s" %s>%s</option>';

	// $users = Sprocket\Cms\Repo\User::all();
	$users = User::all();

	foreach($users as $user)
	{
		$html .= sprintf($tmpl,
			$user->id,
			( $user->id == $selected ) ? 'selected="selected"' :'',
			$user->name
		);
	}
	return $html . '</select>';
});
