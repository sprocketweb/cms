<?php namespace Sprocket\Cms\Helpers;

use \Config;
use \Carbon\Carbon;
use \Lang;

class Time {


	/**
	 * days since date
	 * @param  timestamp $date the date to test
	 * @return [type]       [description]
	 */
	public static function daysAgoStr($date)
	{
		return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
	}

	/**
	 * days since date
	 * @param  timestamp $date the date to test
	 * @return [type]       [description]
	 */
	public static function daysAgo($date, $icon = true)
	{
		// <time title="{{ $page->updated_at }}" datetime="{{ $page->updated_at }}"><i class="fa fa-clock-o"></i>
		// {{ \Carbon\Carbon::createFromTimeStamp(strtotime($page->updated_at))->diffForHumans() }}</time>

		// $tmpl = '<time title="%s" datetime="%s"><i class="fa fa-clock-o"></i>%s</time>';
		$tmpl = '<time title="%s" datetime="%s">%s%s</time>';
		return sprintf($tmpl,
				$date,
				$date,
				$icon?'<i class="fa fa-clock-o"></i> ':'',
				Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans()
				);
	}

	/**
	 * date is after last log in
	 * @param  [type]  $date date to check
	 * @return boolean
	 */
	public static function isNew($date)
	{
		$last_login = new \Carbon(\Auth::user()->last_login);
		$date = new \Carbon($date);

		return ($date->between($last_login, new \Carbon ));
	}


	/*
	 * getDaysDifference()
	 * Used to get the difference of a given date time to date time today
	 * @param
	 *  datetime, $datetime, Given date time
	 *  string, $timezone, Given timezone
	 * @return
	 *  int, Days difference between the given date time and date time today
	 *  NULL,
	 */

	public static function getDaysDiff($datetime = '', $timezone = 'Pacific/Auckland')
	{
		if (date_parse($datetime)) {

			// $Carbon = new \Carbon\Carbon($date2);
			// $last_login = new \Carbon(\Auth::user()->last_login);
			$Carbon = new \Carbon(\Auth::user()->last_login);

			// return days difference from date today
			return $Carbon->diffInHours($Carbon->createFromTimestamp(strtotime($datetime), $timezone));
		}

		// if the given date time is invalid
		echo 'invalid date';
		return NULL;
	}


}
