<?php

$this->app['user:create'] = $this->app->share(function()
{
    return new Sprocket\Cms\Commands\CreateUserCommand();
});

$this->commands('user:create');
