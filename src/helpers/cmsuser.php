<?php namespace Sprocket\Cms\Helpers;

use \Auth;
use \Carbon\Carbon;
use \Session;

class CmsUser {

	public static function isSuper()
	{
		return ( Auth::user() && Auth::user()->super );
	}

	public static function loginMessage($user)
	{
		$prev_login = $user->last_login;
		if ( ! is_null($prev_login))
		{
			$prev_login_human = Carbon::createFromFormat('Y-m-d H:i:s', $prev_login)->diffForHumans();

			$tmpl = 'Welcome. You last logged in <time datetime="%s" title="%s">%s</time>';
			$msg = sprintf( $tmpl, $prev_login, $prev_login, $prev_login_human);
			return Session::flash('login_message', $msg);
		}

		Session::flash('login_message', 'This is your first time here!');

	}

	public static function updateLogin($user)
	{
		$user->last_login = Carbon::now();
		$user->save();
	}

}

