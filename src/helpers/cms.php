<?php namespace Sprocket\Cms\Helpers;

use \Config;
use \Carbon\Carbon;
use \Lang;

class Cms {

	public static function hasSections()
	{
		return Config::get('cms::has_sections',false);
	}

	public static function hasBlog()
	{
		return Config::get('cms::has_blog',false);
	}

	public static function hasMedia()
	{
		return Config::get('cms::has_media',false);
	}

	public static function pagesMenuTitle()
	{
		return Config::get('cms::page.menu.title');
	}

	/**
	 * retrieves message
	 * Cms::msg('messages.dashboard.welcome'
	 * @param  string $key the message
	 * @return string      message
	 */
	public static function msg($key)
	{
		return Lang::get('cms::'.$key);
	}

	/**
	 * message of items count
	 * @param  string $item  the entities
	 * @param  int $count number of item/s
	 * @param  string $class css class
	 * @return string        the message
	 */
	public static function totals( $item, $count, $class = NULL )
	{
		$tmpl = is_null($class) ? '<p>' : "<p class=\"$class\">";
		$tmpl .= 'There %s <span class="badge badge-primary">%d</span> %s.</p>';

		$html = sprintf($tmpl,
			isAre($count),
			$count,
			str_plural($item,$count)
		);

		return $html;
	}

	/**
	 * redirects back to referrer
	 * @param  string  $page   default page
	 * @param  integer $status http code
	 * @return redirect
	 */
	public static function goBack( $page = '/', $status = 302 )
	{
		$back = Redirect::getUrlGenerator()->getRequest()->headers->get('referer');
		if( empty($back) ) $back = $page;
		return Redirect::to($back, $status, Request::header());
	}


	/**
	 * site name
	 * @return string the site name
	 */
	public static function siteName()
	{
		return Config::get('cms::site.name');
	}

}
