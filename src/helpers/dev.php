<?php namespace Sprocket\Cms\Helpers;

use \Config;
use \Carbon\Carbon;
use \Lang;

class Dev {

	/**
	 * test for local environment
	 * @param  string  $env name of environment to test
	 * @return boolean
	 */
	public static function isLocal($env = 'local')
	{
		return app()->environment() == $env;
	}

	/**
	 * test for production environment
	 * @param  string  $env environment name to test
	 * @return boolean
	 */
	public static function isProduction($env = 'production')
	{
		return app()->environment() == $env;
	}

	/**
	 * test if the app is in dev mode
	 * @return boolean
	 */
	public static function isDev()
	{
		$env = app()->environment();
		$local_names = ['local','dev','development','test','testing'];
		$remote_names = ['production','remote'];

		return ( in_array($env, $local_names) OR ! in_array($env, $remote_names) ) ? true : false;
	}


	public static function getEnv()
	{
		return app()->environment();
	}

	/**
	 * opening pre tag
	 * @return string
	 */
	public static function pre($heading=null)
	{
		if (!is_null($heading)) echo "<h1>$heading</h1>\n";
		echo '<pre>';
	}

}
