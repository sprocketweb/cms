<?php namespace Sprocket\Cms\Composers;

use \Sprocket\Cms\Repo\PagesRepo as Pages;
use \Config;

class PagesDashboardComposer {

	/**
	 * Pages Repository
	 *
	 * @var page
	 */
	protected $page;

	public function __construct(Pages $page)
	{
		$this->page = $page;
	}

	public function compose($view)
	{
		$pages = $this->page->getAll();

	    $view->with('pages', $pages);
	}

}
