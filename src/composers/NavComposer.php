<?php namespace Sprocket\Cms\Composers;

use \Config;

class NavComposer {

	public function compose($view)
	{
		$sections = Config::get('cms::nav.sections');

	    $view->with('sections', $sections);
	}

}
