<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('pages');

		Schema::create('pages', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('title',100);
			$table->text('content')->fulltext();
			$table->string('slug',100)->unique()->index();
			$table->string('upload_dir',50)->index();
			$table->string('menu',50)->unique();
			$table->tinyInteger('parent',null)->unsigned()->nullable();
			$table->tinyInteger('order',null)->unsigned()->nullable();
			$table->mediumText('description')->nullable();
			$table->boolean('hidden')->nullable();
			$table->boolean('searchable')->nullable()->default(1);
			$table->boolean('cacheable')->nullable()->default(1);
			$table->boolean('autosave')->nullable()->default(1);
			$table->integer('user_id',null)->unsigned();

			$table->timestamps();
			$table->softDeletes();

		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('pages');
	}

}
