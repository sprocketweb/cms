<?php namespace Sprocket\Cms\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Hash;
use \User;

class CreateUserCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $email = $this->ask('What is the email address? ');
        $password = $this->ask('What do you want the password to be? ');
        $super = $this->ask('Is this a Super User [y/n]? ');

        $user = new User;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->super = ($super == 'y') ? 1 : null;
        $user->save();
        $this->info('The user was successfully created.');
    }
}
