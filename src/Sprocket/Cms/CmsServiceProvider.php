<?php namespace Sprocket\Cms;

use \View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class CmsServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * files to include
	 * outside of the Helper directory
	 * @var array files
	 */
	private $incl_files = [];

	/**
	 * app source directory
	 * used for includes
	 * @var string path
	 */
	private $srcDir = '';

	/**
	 * helper directory for autoloading
	 * @var string path
	 */
	private $helperDir = '';

	/**
	 * aliases to add alias
	 * @var array
	 */
	private $helperAliases = [];

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('sprocket/cms');

		View::addNamespace('cms', __DIR__.'/../../views');

		// auto-include helpers
		$this->helperDir = __DIR__.'/../../helpers'; // helpers directory
		$this->includeHelpers();

		// other files
		$this->incl_files = [
			'handlers/handlers.php',
			'routes.php'
		];

		$this->srcDir = __DIR__.'/../../'; // package src directory
		$this->includeFiles();

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->helperAliases = ['Cms', 'CmsUser', 'Dev', 'Time'];

		$this->loadHelperAliases();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

	/**
	 * include package files
	 * @return null
	 */
	private function includeFiles()
	{
		foreach ($this->incl_files as $include)
		{
			require_once $this->srcDir . $include;
		}
	}

	/**
	 * include all helper files
	 * @return null
	 */
	private function includeHelpers()
	{
	    foreach (glob("{$this->helperDir}/*.php") as $helper)
	    {
	        require_once $helper;
	    }
	}

	/**
	 * loads new aliases for helper files
	 * aliases, filenames, and classnames must be identical
	 * @return null
	 */
	private function loadHelperAliases()
	{
		foreach ($this->helperAliases as $helper)
		{
			AliasLoader::getInstance()->alias($helper, "Sprocket\Cms\Helpers\\$helper");
		}
	}

}
