$('article#help h3').each(function(index){
	var linkid = 'topic'+index;
	var headingText = $(this).text();
	$(this).attr('id', linkid);

	$("#jump").append('<a class="list-group-item" href="#'+linkid+'">'+headingText+'</a>');
});

$("#jump a").on('click',function(){
	$('.hilite').removeClass('hilite');
	var target = $(this).prop("hash");
	$(target).addClass('hilite');
});

$(function() {
  $('#jump a[href*=#]:not([href=#])').click(function(e) {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top + 200
        }, 750);
        e.preventDefault();
      }
    }
  });
});
